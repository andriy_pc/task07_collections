package tree;


public class BinTree<K extends Comparable<K>, V> {
    private StringBuilder inString;

    private class Node {
        Node() {
        }

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            this.left = null;
            this.right = null;
        }

        K key;
        V value;
        Node left;
        Node right;
    }

    private Node root;
    private int valueOfElements = 1;

    public int getValueOfElements() {
        return valueOfElements;
    }

    public void put(K key, V value) {
        Node tmp = new Node(key, value);
        if (root == null) {
            root = tmp;
            return;
        }
        Node parent = findK(key);
        if (key.compareTo(parent.key) > 0) {
            parent.right = tmp;
        } else if (key.compareTo(parent.key) < 0) {
            parent.left = tmp;
        } else {
            parent.value = tmp.value;
        }
        valueOfElements++;
        return;

    }

    public V get(K key) {
        Node tmp = new Node();
        tmp = findK(key);
        if (tmp.key.compareTo(key) == 0) {
            return tmp.value;
        } else {
            return null;
        }
    }

    public boolean delete(K key) {
        Node delete = root;
        Node parent = root;
        ;
        boolean isLeft = true;
        while (key.compareTo(delete.key) != 0) {
            parent = delete;
            if (key.compareTo(delete.key) < 0) {
                delete = delete.left;
                isLeft = true;
            } else if (key.compareTo(delete.key) > 0) {
                delete = delete.right;
                isLeft = false;
            }
            if (delete == null) {
                return false;
            }
        }

        if (delete.left == null && delete.right == null) {
            if (delete == root) {
                root = null;
            } else if (isLeft) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (delete.right == null) {
            if (delete == root) {
                root = delete.left;
            } else if (isLeft) {
                parent.left = delete.left;
            } else {
                parent.right = delete.left;
            }
        } else if (delete.left == null) {
            if (delete == root) {
                root = root.right;
            } else if (isLeft) {
                parent.left = delete.right;
            } else {
                parent.right = delete.right;
            }
        } else {
            Node successor = getSuccessor(delete.right);
            if (delete == root) {
                root = successor;
            } else if (isLeft) {
                parent.left = successor;
            } else {
                parent.right = successor;
            }
            successor.left = delete.left;
        }

        valueOfElements--;
        return true;
    }

    private Node findK(K key) {
        if (root == null) {
            return null;
        }
        Node iterator = root;
        Node required = root;
        while (iterator != null) {
            required = iterator;
            if (key.compareTo(iterator.key) > 0) {
                iterator = iterator.right;
            } else if (key.compareTo(iterator.key) < 0) {
                iterator = iterator.left;
            } else {
                iterator = null;
            }
        }
        return required;
    }

    private Node getSuccessor(Node removableRight) {
        if (removableRight.left == null) {
            return removableRight;
        }
        Node iterator = removableRight;
        Node successor = removableRight;
        Node successorParent = removableRight;
        while (iterator != null) {
            successorParent = successor;
            successor = iterator;
            iterator = iterator.left;
        }
        successorParent.left = successor.right;
        successor.right = removableRight;
        return successor;
    }

    private String walk(Node node) {//ARB
        if (node != null) {
            walk(node.left);
            inString.append(node.value + " ");
            walk(node.right);
        }
        return inString.toString();
    }

    /*private String walk (Node node) {//RAB
        if(node != null) {
            inString.append(node.value+" ");
            walk(node.left);
            walk(node.right);
        }
        return inString.toString();
    }*/
    @Override
    public String toString() {
        inString = new StringBuilder();
        return walk(root);
    }
    /*private void walk(Node node) {//ABR
        if(node != null) {
            System.out.print(node.value+" ");
            walk(node.right);
            walk(node.left);
        }
    }*/
    public static void main(String[] args) {
        BinTree<Integer, Integer> bi = new BinTree<Integer, Integer>();
        bi.put(5, 5);
        bi.put(4, 4);
        bi.put(6, 6);
        System.out.println(bi);

        BinTree<TestClass, Integer> bt = new BinTree<TestClass, Integer>();
        bt.put(new TestClass(5), 5);
        bt.put(new TestClass(6), 6);
        bt.put(new TestClass(3), 3);
        System.out.println(bt);
    }
}



