package tree;

public class TestClass implements Comparable<TestClass> {
    private int key;
    public TestClass(int k) {
        key = k;
    }
    public int compareTo(TestClass t2) {
        return key < t2.key ? -1 : 1;
    }

    public String toString() {
        return key+"| ";
    }
}
