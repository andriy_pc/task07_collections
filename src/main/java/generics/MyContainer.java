package generics;

public class MyContainer<T> {
    private int index = 0;
    private Object[] contain = new Object[15];
    public T get(int position) {
        return (T) contain[position];
    }
    public void add(T t) {
        if((index+1) == contain.length-1) {
            contain = resizeArr(index);
        }
        contain[index] = t;
        ++index;
    }
    private Object[] resizeArr(int size) {
        Object[] result = new Object[2*size];
        for(int i = 0; i < size; ++i) {
            result[i] = contain[i];
        }
        return result;
    }
}
