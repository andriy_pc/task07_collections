package generics;

import java.util.Comparator;

public class MyCompareContainer implements Comparable<MyCompareContainer>, Comparator<MyCompareContainer> {
    private String s1;
    private String s2;
    private boolean b1;
    public MyCompareContainer() {
        this.s1 = "a";
        this.s2 = "b";
        this.b1 = true;
    }
    public MyCompareContainer(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
        this.b1 = true;
    }
    public MyCompareContainer(String s1, String s2, boolean useS1) {
        this.s1 = s1;
        this.s2 = s2;
        this.b1 = useS1;
    }
    public int compareTo(MyCompareContainer t) {
        if(b1) {
            return s1.compareTo(t.s1);
        }
        else {
            return s2.compareTo(t.s2);
        }
    }
    public String toString() {
        return "s1:" + s1 + " s2:" + s2;
    }
    public String getS1() {
        return this.s1;
    }

    @Override
    public int compare(MyCompareContainer o1, MyCompareContainer o2) {
        return o1.compareTo(o2);
    }
}
