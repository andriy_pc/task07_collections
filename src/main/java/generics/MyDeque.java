package generics;

import java.util.Collection;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/*I didn't have much time to make it looks good,
* so sorry for my sh*t code...*/
public class MyDeque<E> implements java.util.Deque<E> {
    private Logger log = LogManager.getLogger();
    private Object[] elements = new Object[15];
    private int head = 6;
    private int tail = 7;

    @Override
    public void addFirst(E e) {
        if(head == 0) {
            log.trace("there is no more space!");
            System.exit(0);
        }
        elements[head] = e;
        head--;
    }

    @Override
    public void addLast(E e) {
        if(tail == 14) {
            log.trace("there is no more space!");
            System.exit(0);
        }
        elements[tail] = e;
        tail++;
    }

    @Override
    public E peekFirst() {
        if(head == tail) {
            log.trace("Dec is empty!");
            return null;
        }
        return (E)elements[head+1];
    }

    @Override
    public E peekLast() {
        if(tail == head) {
            log.trace("Dec is empty!");
            return null;
        }
        return (E)elements[tail-1];
    }

    @Override
    public E pollFirst() {
        if(head == tail) {
            log.trace("Dec is empty!");
            return null;
        }
        head++;
        E result = (E) elements[head];
        elements[head] = null;
        return result;
    }

    @Override
    public E pollLast() {
        if((head == tail)) {
            log.trace("Dec is empty!");
            return null;
        }
        tail--;
        E result = (E) elements[tail];
        elements[tail] = null;
        return result;
    }

    @Override
    public boolean isEmpty() {
        log.trace("head: " + head);
        log.trace("tail: " + tail);
        return head == tail-1;
    }

    @Override
    public void clear() {
        head = 6;
        tail = 7;
    }
    @Override
    public boolean offerFirst(E e) {
        return false;
    }

    @Override
    public boolean offerLast(E e) {
        return false;
    }

    @Override
    public E removeFirst() {
        return null;
    }

    @Override
    public E removeLast() {
        return null;
    }

    @Override
    public E getFirst() {
        return null;
    }

    @Override
    public E getLast() {
        return null;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(E e) {
        return false;
    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public E remove() {
        return null;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void push(E e) {

    }

    @Override
    public E pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }
}
