package generics;

import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/*PriorityQueue uses Comparator to sort elements in
 * storage of data, to give them priority. It also holds elements
 * in the way they were inserted, if the comparator is not given
 * as a parameter.;
 * So i don't use comparator, i just
 * use second option
 *
 * I didn't have much time to make it work well,
 * so excuse me for my simplifying*/

public class MyPriorityQueue<T> extends AbstractQueue<T> {
    private static Logger log = LogManager.getLogger();
    private int size = 15;
    private int modCount = 0;
    private Object[] queue = new Object[size];
    private Comparator<? super T> comparator;
    @Override
    public Iterator<T> iterator() {
        return new Iterator<>(){
            @Override
            public boolean hasNext() {
                return modCount > 0;
            }
            @Override
            public T next() {
                return (T) queue[--modCount];
            }
        };
    }

    @Override
    public int size() {
        return modCount+1;
    }

    @Override
    public boolean offer(T e) {
        if(modCount+1 == 15) {
            log.trace("No more elements will be inserted");
        }
        queue[modCount++] = e;
        return true;
    }

    @Override
    /*Retrieves and removes the head of this queue*/
    public T poll() {
        if(modCount == 0) {
            log.fatal("The queue is EMPTY!");
            return null;
        }
        T result = (T) queue[--modCount];
        queue[modCount] = null;
        return result;
    }
    @Override
    /*Retrieves, but does not remove, the head of this queue*/
    public T peek() {
        if(modCount == 0) {
            log.fatal("The queue is EMPTY!");
            return null;
        }
        return (T)  queue[modCount-1];
    }
}

