package generics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Test {
    private static Logger log = LogManager.getLogger();

    public static void main(String[] args) {
        //First test performance of my container
        long StartTimeMc;
        long EndTimeMc;
        long resultMc;

        long StartTimeLc;
        long EndTimeLc;
        long resultLc;
        MyContainer<String> mc = new MyContainer<>();
        ArrayList<String> lc = new ArrayList<>();
        StartTimeMc = System.nanoTime();
        for(int i = 0; i < 1000; ++i) {
            mc.add(Integer.toString(i));
        }
        EndTimeMc = System.nanoTime();
        resultMc = EndTimeMc - StartTimeMc;

        StartTimeLc = System.nanoTime();
        for(int i = 0; i < 1000; ++i) {
            lc.add(Integer.toString(i));
        }
        EndTimeLc = System.nanoTime();
        resultLc = EndTimeLc - StartTimeLc;

        String FirstTaskResult = resultMc < resultLc ? "My is slower(" :
                "My is faster)";
        log.trace(FirstTaskResult);

        //Then i perform task with my class with 2 string to sort in List
        ArrayList<MyCompareContainer> list = customGenerator(15, true);
        list.sort(null);
        StringBuilder SecondTaskResult = new StringBuilder();
        for(MyCompareContainer i : list) {
            SecondTaskResult.append(i + " ");
        }
        log.trace(SecondTaskResult.toString());

        StringBuilder SecondBTaskResult = new StringBuilder();
        ArrayList<MyCompareContainer> list2 = customGenerator(15, false);
        list2.sort(null);
        for(MyCompareContainer i : list2) {
            SecondBTaskResult.append(i + " ");
        }
        log.trace(SecondBTaskResult.toString());
        MyCompareContainer toUseInBinSearch = new MyCompareContainer("e", "e");
        MyCompareContainer[] mcc = new MyCompareContainer[] {
                toUseInBinSearch,
                new MyCompareContainer("b", "b"),
                new MyCompareContainer("d", "d"),
                new MyCompareContainer("a", "a"),
                new MyCompareContainer("c", "c")
        };
        MyCompareContainer comparator = new MyCompareContainer();
        Arrays.sort(mcc, comparator);
        //Should be 0, because the element i search was first inserted
        log.trace(Arrays.binarySearch(mcc, comparator, toUseInBinSearch));
    }

    public static ArrayList<MyCompareContainer> customGenerator(int quantity, boolean useS1) {
        ArrayList<MyCompareContainer> result = new ArrayList<>();
        Random rand = new Random(47);
        String s1;
        String s2;
        for(int i = 0; i < quantity; ++i) {
            s1 = (char)('a'+rand.nextInt(20)) + "";
            s2 = (char)('a'+rand.nextInt(20)) + "";
            result.add(new MyCompareContainer(s1, s2, useS1));
        }
        return result;
    }
}
