package generics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestDeque {
    private static final Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        MyDeque<Integer> md = new MyDeque<>();
        md.addFirst(5);
        md.addFirst(1);
        md.addFirst(2);
        md.addLast(3);
        md.addLast(4);
        log.trace(md.isEmpty());
        log.trace(md.pollLast());
        log.trace(md.pollLast());
        log.trace(md.pollLast());
        log.trace(md.pollLast());
        log.trace(md.pollLast());
        log.trace(md.isEmpty());
    }
}
