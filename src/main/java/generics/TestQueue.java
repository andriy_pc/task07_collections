package generics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//Small test to see if it works
public class TestQueue {
    private static Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        MyPriorityQueue<Integer> mpq = new MyPriorityQueue<>();
        mpq.offer(5);
        mpq.offer(6);
        mpq.offer(1);
        mpq.offer(2);
        StringBuilder sb = new StringBuilder("result: \n");
        for(Integer i : mpq) {
            sb.append(i+" ");
        }
        log.trace(sb.toString());
    }
}
