package logic;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogicTasks {
/*I guess it was not necessary to use MVC
* scheme in this logic tasks*/
    private static Logger log = (Logger) LogManager.getLogger();

    public static int[] taskA(int[] fir, int sec[]) {
        int resultLength = Math.max(fir.length, sec.length);
        int[] result = new int[resultLength];
        for(int i = 0; i < fir.length; i++) {
            for(int j = 0; j < sec.length; j++) {
                if(fir[i] == sec[j]) {
                    result[i] = fir[i];
                    break;
                }
            }
        }
        StringBuilder res = new StringBuilder("Task A\n");
        for(int i : result) {
            res.append(i+":");
        }
        res.append("\n");
        log.trace(res.toString());
        return result;
    }

    public static int[] taskAb(int[] fir, int sec[]) {
        //Array of common elements in fir and sec
        int[] common = taskA(fir, sec);
        int[] result = new int[fir.length+sec.length];
        int k = 0;
        for (int i = 0; i < fir.length; i++) {
            for (int j = 0; j < common.length; ++j) {
                if(common[j] == fir[i]) {
                    break;
                }
                if (j == common.length-2) {
                    result[k] = fir[i];
                    result[++k] = sec[i];
                    k++;
                }
            }
        }
        StringBuilder res = new StringBuilder("Task ab\n");
        for(int i : result) {
            res.append(i+":");
        }
        res.append("\n");
        log.trace(res.toString());
        return result;
    }
    /*I create counter for all the numbers
    * And if this counter <= 2, then
    * I add element with this counter to
    * the result arr*/
    public static int[] taskB(int[] arr) {
        int[] result = new int[arr.length];
        int k = 0;
        for(int i = 0; i < arr.length; ++i) {
            int tmp = arr[i];
            int counter = 1;
            for(int j = 0; j < arr.length; ++j) {
                if(j == i) {
                    counter--;
                }
                if(arr[j] == tmp) {
                    ++counter;
                }
            }
            if(counter <= 2) {
                result[k++] = tmp;
            }
        }
        StringBuilder res = new StringBuilder("Task B\n");
        for(int i : result) {
            res.append(i+":");
        }
        res.append("\n");
        log.trace(res.toString());
        return result;
    }

    public static int[] taskC(int[] arr) {
        int arrLength = arr.length;
        int k = 0;
        int[] result = new int[arrLength];
        //Despite i have to collect only one number
        //which has analogs, the first number
        //is appropriate
        result[0] = arr[0];
        for (int i = 1; i < arrLength; i++) {
            if (arr[i] != result[k]) {
                result[++k] = arr[i];
            } else if (arr[i] == result[k]) {
                int ignore = arr[i];
                while (arr[i] == ignore) {
                    if (i++ != ignore) {
                        break;
                    }
                }
            }
        }
        StringBuilder res = new StringBuilder("Task C\n");
        for(int i : result) {
            res.append(i+":");
        }
        res.append("\n");
        log.trace(res.toString());
        return result;
    }

        public static void main(String[] args) {
            int[] a = new int[] {1, 2, 2, 2, 2, 4, 4, 5, 5, 6, 7, 8, 8, 8, 9};
            int[] ab = new int[] {6, 5, 7, 1, 3};
            int[] b = new int[] {6, 7, 8, 9, 10};
            int[] A = taskA(ab, b);
            int[] Ab = taskAb(ab, b);
            int[] B = taskB(a);
            int[] C = taskC(a);
    }
}
