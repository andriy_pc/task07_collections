package mgame;


public class Model {
    private int hero;
    private int[] hall;
    private int deathDors;
    private MyRandom rand;

    public Model() {
        hero = 25;
        hall = new int[10];
        rand = new MyRandom();

        hall = generateHall();
    }

    private int[] generateHall() {
        int[] result = new int[10];
        int sign;
        int val;
        for(int i = 0; i < 10; ++i) {
            sign = rand.nextSign();
            if(sign < 0) {
                val = rand.nextInt(5, 100);
            } else {
                val = rand.nextInt(10, 80);
            }
            result[i] = sign*val;
        }
        return result;
    }

    public String makeTable() {
        StringBuilder table = new StringBuilder();
        table.append(String.format(
                "dors:|%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d|\n",
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        table.append(String.format(
                "pnts:|%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d| |%10d|",
                hall[0], hall[1], hall[2], hall[3], hall[4],
                hall[5], hall[6], hall[7], hall[8], hall[9]));
        String result = table.toString();
        View.display(result);
        return result;
    }
    private int DeathDoors(int door) {
        int counter = 0;
        while(door != 10) {
            counter += DeathDoors(door+1);
            if(hall[door] + getHeroHp() < 0) {
                counter += 1;
            }
            else if(hall[door] + getHeroHp() < 0) {
                counter += 0;
            }
            return counter;
        }
        return counter;
    }

    public void countDeathDoors() {
        View.display(DeathDoors(0));
    }

    public int getHeroHp() {
        return hero;
    }

    public String getLucky() {
        int heroesHP = getHeroHp();
        StringBuilder resultL = new StringBuilder("door should be opened first: ");
        StringBuilder resultD = new StringBuilder("doors with enemy to open after: ");
        for (int i = 0; i < 10; ++i) {
            heroesHP += hall[i];
            if(hall[i] > 0) {
                resultL.append(i+1);
                resultL.append(", ");
            } else if(hall[i] < 0) {
                resultD.append(i+1);
                resultD.append(", ");
            }
        }
        resultL.append("\n");
        resultL.append(resultD);
        resultL.append("\n");
        if(heroesHP > 0) {
            resultL.append("Hero got lucky, he will get through");
        } else {
            resultL.append("Hero won't get through...");
        }
        String result = resultL.toString();
        View.display(result);
        return result;
    }
}
