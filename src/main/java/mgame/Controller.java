package mgame;

import java.io.IOException;
import java.util.Scanner;

public class Controller {
    private Model model;
    private Scanner in;
    private String mainMenu = "Please select action:\n"
            +
            "0) quit\n"
            +
            "1) show cute table with doors\n"
            +
            "2) count doors with death inside\n"
            +
            "3) See if it is possible to get through";
    public Controller(Model m) {
        model = m;
        in = new Scanner(System.in);

    }
    public void run() {
        View.display(mainMenu);
        int choice = in.nextInt();
        while(choice != 0) {
            switch(choice) {
                case 0 : System.exit(0);
                case 1 : model.makeTable(); break;
                case 2 : model.countDeathDoors(); break;
                case 3: model.getLucky(); break;
            }
            View.display(mainMenu);
            choice = in.nextInt();
        }
    }
}
