package mgame;

import java.util.Random;

public class MyRandom {
    int nextInt(int a, int b) {
        return (int) (Math.random()*(b-a+1) + a);
    }
    int nextSign() {
        return Math.random() <= 0.5 ? -1 : 1;
    }
}
